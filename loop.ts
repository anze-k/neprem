const Database = require('better-sqlite3');
const Telegraf = require('telegraf');
const puppeteer = require('puppeteer');

const bot = new Telegraf('1122573563:AAH1kEi6xvuCK8qB_uVU8sG8aAc6ALP0Duo');
const channelId = '-411056721';
const db = new Database('neprem.db', {});


interface Filter {
    id?: string,
    owner: string,
    url: string,
    type: string
}

bot.on('message', async (ctx) => {
    if (!ctx.message.text) return; // why is this even needed?
    if (ctx.message.text.startsWith('!f')) {
        try {
            const params = ctx.message.text.split(' ');

            if (params.length != 3) { 
                ctx.reply(`!f filternaslov(web) prodaja/oddaja`);
                ctx.reply(`Primer: !f https://www.nepremicnine.net/oglasi-oddaja/ljubljana-mesto/stanovanje/garsonjera,1-sobno/cena-od-300-do-450-eur-na-mesec/?nadst%5B0%5D=vsa&nadst%5B1%5D=vsa oddaja`);
                return;
            }
            else {
                if (params[2] != "oddaja" && params[2] != "prodaja") {
                    ctx.reply(`Tip mora biti oddaja ali prodaja!`);
                    return;
                }

                const name = ctx.message.from.first_name;
                const filterWeb = params[1];
                const filterType = params[2];
                
                ctx.reply(`Shranjujem trenutne izbore`);
                run({ owner: name, url: filterWeb, type: filterType}, false).then(
                    result => {
                        ctx.reply(`Ok!`);
                        db.prepare('INSERT INTO filters (owner, url, type) values (?, ?, ?)').run(name, filterWeb, filterType);
                    }
                );
            }
          } catch (err) {
            ctx.reply(`Napaka?`);
            console.log(err);
            return;
          }
    }

    if (ctx.message.text.startsWith('!del')) {
        try {
            await db.prepare(`delete from filters where owner = ?`).run(ctx.message.from.first_name);
            ctx.reply(`Filtri zbrisani za uporabnika ${ctx.message.from.first_name}`);
          } catch (err) {
            ctx.reply(`Napaka?`);
            console.log(err);
            return;
          }
    }

    if (ctx.message.text.startsWith('!h')) {
        ctx.reply("!f [filterlink] [prodaja/oddaja] | !del")
    }
});

bot.startPolling();

(async() => {
    await db.exec(`CREATE TABLE IF NOT EXISTS offers (id INTEGER PRIMARY KEY ASC, web TEXT, name TEXT, size TEXT, price TEXT, owner TEXT);`);
    await db.exec(`CREATE TABLE IF NOT EXISTS filters (id INTEGER PRIMARY KEY ASC, owner TEXT, url TEXT, type TEXT);`);

    await runForAllFilters();
    setInterval(runForAllFilters, 600000);
})();


async function runForAllFilters() {
    const browser = await puppeteer.launch({args: ['--no-sandbox',
        "--no-zygote"]});

    const filters = await db.prepare('SELECT * from filters').all();

    for (const filter of filters) {
        await run(filter, browser);
    }

    await browser.close();
}


async function run(filter: Filter, browser, verbose = true){
    console.log("Starting!");

    try {
        await scrapePage(browser, filter, verbose);
    } catch (ex) {
        console.log("Failed!");
        console.log(ex);
    }
}

async function scrapePage(browser, filter: Filter, verbose) {
    console.log("Starting filter " + filter.owner);
    const page = await browser.newPage();
    await page.setDefaultNavigationTimeout(0); 
    await page.goto(filter.url, {waitUntil: 'networkidle2'});
    await page.waitForSelector('.seznam h2');
    const titles = await page.$$('.seznam h2');
    const sizes = await page.$$('.velikost');
    const prices = await page.$$('span.cena');
    
    for (let i = 0; i < titles.length; i++) {
        try {
            const title = await page.evaluate(el => el.innerText, titles[i]);
            let titleWeb = await titles[i].$eval('a', el => el.getAttribute('href'));
            if (!titleWeb || !titleWeb.includes("oglasi-"+filter.type)) continue; // avoid fake and old ads
           
            titleWeb = titleWeb.slice(8 + filter.type.length, titleWeb.length)
            titleWeb = "/oglasi-" + filter.type + titleWeb;

            const size = await page.evaluate(el => el.innerText, sizes[i]);
            const price = await page.evaluate(el => el.innerText, prices[i]);
            console.log(`${filter.owner} - *${title}*: ${size} za ${price.replace("/", " na ")}!`);

            const result = await db.prepare('SELECT * from offers where web = ? and owner = ?').all(titleWeb, filter.owner);
            if (result.length > 0) continue;

            await db.prepare('INSERT INTO offers (web, name, size, price, owner) values (?, ?, ?, ?, ?)').run(titleWeb, title, size, price, filter.owner);
            
            let pricePerM;
            if (filter.type == "prodaja") {
                try {
                    const sizeNum = parseFloat(size.trim().replace(",", ".").replace("m2", ""));
                    const priceNum = parseFloat(price.trim().replace(".", "").replace(".", "").split(",")[0]);
                    pricePerM = Math.round(priceNum / sizeNum);
                } catch(ex) {
                    console.log(ex)
                }
            }
            
            if (verbose) {
                const msg = `${filter.owner} - *${title}*: ${size} za ${price.replace("/", " na ")} (${ pricePerM ? pricePerM + " eur/m2" : ""})!`
                await bot.telegram.sendMessage(channelId, msg, Telegraf.Extra.markdown());
                await bot.telegram.sendMessage(channelId, 'https://www.nepremicnine.net' + titleWeb);
            }

            await new Promise(resolve => setTimeout(resolve, 1000));
            
        } catch (ex) {
            console.log("error yo", ex)
            continue;
        }
    }

    const nextButton = await page.$('.paging_next');
    if (nextButton !== null) {
        const url = await nextButton.$eval('a', el => el.getAttribute('href'));
        filter.url = 'https://www.nepremicnine.net' + url;
        await page.close();
        await scrapePage(browser, filter, verbose);
    }

    console.log("Done filter " + filter.owner);
}
